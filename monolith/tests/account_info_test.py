import requests

# Base URL for the attendees microservice
BASE_URL = "http://localhost:8001"


# Step 1: Create an attendee without an account and check "has_account" value
def test_attendee_without_account():
    # Create an attendee without an account
    email = "test@example.com"
    attendee_data = {
        "email": email,
        "name": "John Doe",
        "company_name": "Acme Inc",
        # Add other attendee data as needed
    }
    response = requests.post(f"{BASE_URL}/attendees", json=attendee_data)
    attendee_id = response.json()["id"]
    has_account = response.json()["has_account"]

    # Assert that the response JSON contains "has_account": false
    assert has_account is False

    # Store the attendee ID for later use
    return attendee_id


# Step 2: Create a new account with the email from the previous step
def test_create_account(attendee_id):
    # Create a new account using the attendee ID
    account_data = {
        "attendee_id": attendee_id,
        # Add other account data as needed
    }
    response = requests.post(f"{BASE_URL}/accounts", json=account_data)

    # Assert that the response status is successful
    assert response.status_code == 201


# Step 3: Get the attendee details and check "has_account" value
def test_attendee_with_account(attendee_id):
    # Retrieve the attendee details
    response = requests.get(f"{BASE_URL}/attendees/{attendee_id}")
    has_account = response.json()["has_account"]

    # Assert that the response JSON contains "has_account": true
    assert has_account is True


# Run the test functions
if __name__ == "__main__":
    attendee_id = test_attendee_without_account()
    test_create_account(attendee_id)
    test_attendee_with_account(attendee_id)
