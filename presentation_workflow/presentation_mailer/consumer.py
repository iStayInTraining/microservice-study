import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    message = json.loads(body)
    presenter_name = message['presenter_name']
    presenter_email = message['presenter_email']
    title = message['title']

    subject = "Your presentation has been accepted"
    body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"
    send_mail(
        subject,
        body,
        'admin@conference.go',
        [presenter_email],
        fail_silently=False,
    )

    print(f"Received approval message: {body}")


def process_rejection(ch, method, properties, body):
    message = json.loads(body)
    presenter_name = message['presenter_name']
    presenter_email = message['presenter_email']
    title = message['title']

    subject = "Your presentation has been rejected"
    body = f"{presenter_name}, we regret to inform you that your presentation {title} has been rejected"
    send_mail(
        subject,
        body,
        'admin@conference.go',
        [presenter_email],
        fail_silently=False,
    )

    print(f"Received rejection message: {body}")


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq', port=5672)
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)

# import json
# import pika
# from pika.exceptions import AMQPConnectionError
# import django
# import os
# import sys
# import time
# from django.core.mail import send_mail


# sys.path.append("")
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
# django.setup()


# def process_approval(ch, method, properties, body):
#     # Decode the message from JSON format
#     message = json.loads(body)

#     # Extract the presenter's details from the message
#     presenter_name = message['presenter_name']
#     presenter_email = message['presenter_email']
#     title = message['title']

#     # Compose and send the approval email
#     subject = "Your presentation has been accepted"
#     body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"
#     send_mail(
#         subject,
#         body,
#         'admin@conference.go',
#         [presenter_email],
#         fail_silently=False,
#     )

#     print(f"Received approval message: {body}")


# parameters = pika.ConnectionParameters(host='rabbitmq', port=5672)
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# channel.queue_declare(queue='presentation_approvals')
# channel.basic_consume(
#     queue='presentation_approvals',
#     on_message_callback=process_approval,
#     auto_ack=True,
# )
# channel.start_consuming()


# def process_rejection(ch, method, properties, body):
#     # Decode the message from JSON format
#     message = json.loads(body)

#     # Extract the presenter's details from the message
#     presenter_name = message['presenter_name']
#     presenter_email = message['presenter_email']
#     title = message['title']

#     # Compose and send the rejection email
#     subject = "Your presentation has been rejected"
#     body = f"{presenter_name}, we regret to inform you that your presentation {title} has been rejected"
#     send_mail(
#         subject,
#         body,
#         'admin@conference.go',
#         [presenter_email],
#         fail_silently=False,
#     )

#     print(f"Received rejection message: {body}")


# channel.queue_declare(queue='presentation_rejections')
# channel.basic_consume(
#     queue='presentation_rejections',
#     on_message_callback=process_rejection,
#     auto_ack=True,
# )
# channel.start_consuming()
