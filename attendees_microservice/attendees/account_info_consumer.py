from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_account(ch, method, properties, body):
    content = json.loads(body.decode())
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)

    if is_active:
        AccountVO.objects.update_or_create(
            email=email,
            defaults={
                "first_name": first_name,
                "last_name": last_name,
                "is_active": is_active,
                "updated": updated
            }
        )
    else:
        AccountVO.objects.filter(email=email).delete()

# Infinite loop to consume messages
while True:
    try:
        # Create the pika connection parameters
        parameters = pika.ConnectionParameters("rabbitmq")

        # Create a blocking connection with the parameters
        connection = pika.BlockingConnection(parameters)

        # Open a channel
        channel = connection.channel()

        # Declare a fanout exchange named "account_info"
        channel.exchange_declare(exchange="account_info", exchange_type="fanout")

        # Declare a randomly-named queue
        result = channel.queue_declare(queue="", exclusive=True)

        # Get the queue name of the randomly-named queue
        queue_name = result.method.queue

        # Bind the queue to the "account_info" exchange
        channel.queue_bind(exchange="account_info", queue=queue_name)

        # Do a basic_consume for the queue name that calls the update_account function
        channel.basic_consume(queue=queue_name, on_message_callback=update_account, auto_ack=True)

        # Tell the channel to start consuming
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ. Retrying in a few seconds...")
        time.sleep(2)
